local function createShadows()
    BSHADOWS = {}
    
    --The original drawing layer
    BSHADOWS.RenderTarget = GetRenderTarget("bshadows_original_" .. ScrW(), ScrW(), ScrH())
    
    --The matarial to draw the render targets on
    BSHADOWS.ShadowMaterial = CreateMaterial("bshadows_" .. ScrW(),"UnlitGeneric",{
        ["$translucent"] = 1,
        ["$vertexalpha"] = 1,
        ["alpha"] = 1
    })

    BSHADOWS.CreatedShadowMaterials = {}
    
    --Call this to begin drawing a shadow
    BSHADOWS.BeginShadow = function( uniqueID, areaX, areaY, areaEndX, areaEndY )
        --Set the render target so all draw calls draw onto the render target instead of the screen
        render.PushRenderTarget(BSHADOWS.RenderTarget)
    
        --Clear is so that theres no color or alpha
        render.OverrideAlphaWriteEnable(true, true)
        render.Clear(0,0,0,0)
        render.OverrideAlphaWriteEnable(false, false)
    
        if( BSHADOWS.CreatedShadowMaterials[uniqueID] and areaX and areaY and areaEndX and areaEndY ) then
            render.SetScissorRect( areaX, areaY, areaEndX, areaEndY, true )

            if( not BSHADOWS.CreatedShadowMaterials[uniqueID][4] ) then
                BSHADOWS.CreatedShadowMaterials[uniqueID][4] = areaX
                BSHADOWS.CreatedShadowMaterials[uniqueID][5] = areaY
                BSHADOWS.CreatedShadowMaterials[uniqueID][6] = areaEndX
                BSHADOWS.CreatedShadowMaterials[uniqueID][7] = areaEndY
            end
        end

        --Start Cam2D as where drawing on a flat surface 
        cam.Start2D()
    
        --Now leave the rest to the user to draw onto the surface
    end
    
    --This will draw the shadow, and mirror any other draw calls the happened during drawing the shadow
    BSHADOWS.EndShadow = function( uniqueID, x, y, intensity, spread, blur, opacity, direction, distance, _shadowOnly )
        
        -- Set default opcaity
        opacity = opacity or 255
        direction = direction or 0
        distance = distance or 0
        _shadowOnly = _shadowOnly or false
    
        if( not BSHADOWS.CreatedShadowMaterials[uniqueID] ) then
            local shadowRenderTarget = GetRenderTarget("bshadows_shadow_" .. ScrW() .. "_id_" .. uniqueID, ScrW(),  ScrW(), ScrH())
            -- Copy this render target to the other
            render.CopyRenderTargetToTexture(shadowRenderTarget)
        
            --Blur the second render target
            if blur > 0 then
                render.OverrideAlphaWriteEnable(true, true)
                render.BlurRenderTarget(shadowRenderTarget, spread, spread, blur)
                render.OverrideAlphaWriteEnable(false, false) 
            end

            BSHADOWS.CreatedShadowMaterials[uniqueID] = { CreateMaterial("bshadows_grayscale_" .. ScrW() .. "_id_" .. uniqueID,"UnlitGeneric",{
                ["$translucent"] = 1,
                ["$vertexalpha"] = 1,
                ["$alpha"] = 1,
                ["$color"] = "0 0 0",
                ["$color2"] = "0 0 0"
            }), x, y }

            BSHADOWS.CreatedShadowMaterials[uniqueID][1]:SetTexture('$basetexture', shadowRenderTarget)
            BSHADOWS.CreatedShadowMaterials[uniqueID][1]:SetFloat("$alpha", opacity/255)
        end

        --First remove the render target that the user drew
        render.PopRenderTarget()

        --Now update the material to what was drawn
        BSHADOWS.ShadowMaterial:SetTexture('$basetexture', BSHADOWS.RenderTarget)
        
        --Work out shadow offsets
        local shadowTable = BSHADOWS.CreatedShadowMaterials[uniqueID]
        local xOffset = math.sin(math.rad(direction)) * distance 
        local yOffset = math.cos(math.rad(direction)) * distance

        if( shadowTable[4] ) then 
            render.SetScissorRect( shadowTable[4], shadowTable[5]+1, shadowTable[6], shadowTable[7]-1, true )
        end
        render.SetMaterial(shadowTable[1])
        for i = 1 , math.ceil(intensity) do
            render.DrawScreenQuadEx(xOffset+(x-shadowTable[2]), yOffset+(y-shadowTable[3]), ScrW(), ScrH())
        end
        if( shadowTable[4] ) then render.SetScissorRect( 0, 0, 0, 0, false ) end
    
        if not _shadowOnly then
            --Now draw the original
            BSHADOWS.ShadowMaterial:SetTexture('$basetexture', BSHADOWS.RenderTarget)
            render.SetMaterial(BSHADOWS.ShadowMaterial)
            render.DrawScreenQuad()
        end
    
        cam.End2D()

        render.SetScissorRect( 0, 0, 0, 0, false )
    end
end
createShadows()

hook.Add( "OnScreenSizeChanged", "BShadows.OnScreenSizeChanged", createShadows )